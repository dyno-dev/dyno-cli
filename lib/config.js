module.exports.almondFile = "almond.json";
module.exports.commandName = "almond";
module.exports.companyName = "Almond"
module.exports.repoLocation = "https://gitlab.com/almond-valkyries/almond-valkyries"
module.exports.callbackURL = "http://localhost:3001/api/v1/auth/github/callback"
module.exports.docsSite = "https://docs.almond.com";
module.exports.crud = ["index", "create", "read", "update", "destroy"];
